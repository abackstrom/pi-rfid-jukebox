"use strict";

const EventEmitter = require('events').EventEmitter;

const tags = require('./tags');

module.exports.PlayLog = class PlayLog extends EventEmitter {
  constructor() {
    super();
    this.last_tag = undefined;
    this.log = [];
    this.log_size = 10;
  }

  updateLog(tag) {
    this.last_tag = tag;

    // early exit for NotFoundTag, these don't show
    // up in the play history
    if (tag instanceof tags.NotFoundTag) {
      this.emit('update', tag);
      return;
    }

    this.log.unshift(tag);
    if (this.log.length > this.log_size) {
      this.log.pop();
    }

    this.emit('update', tag);
  }

  getLastTag() {
    if (this.log.length == 0) {
      return;
    }

    return this.log[0].tag;
  }

  getLog() {
    return this.log;
  }
}

// vim:ts=2 sw=2 et:
