window.onload = function() {
  var play_log = document.getElementById('play-log');
  var last_tag = document.getElementById('last-tag');

  function updateLog(data) {
    last_tag.innerHTML = data.tag;
    play_log.innerHTML = data.html;
  }

  var host = window.document.location.host.replace(/:.*/, '');
  var ws;

  function connectWebsocket() {
    console.log('Attempting WebSocket connection...');
    ws = new WebSocket('ws://' + host + ':' + jukebox.port);
    ws.addEventListener('open', function(event) {
      console.log('WebSocket connection opened.');
    });
    ws.addEventListener('message', function(event) {
      updateLog(JSON.parse(event.data));
    });
  }

  setInterval(function() {
    if (ws.readyState == WebSocket.CLOSED) {
      connectWebsocket();
    }
  }, 1000);

  connectWebsocket();
};
