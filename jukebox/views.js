"use strict";

const mustache = require('mustache');
const readFileSync = require('fs').readFileSync;

var template_dir = undefined;

class View {
    constructor() {
        this.template = readFileSync(template_dir + this.path(), {encoding: 'utf8'})
        mustache.parse(this.template)
    }

    render(vars) {
        return mustache.render(this.template, vars, this.partials());
    }

    path() {
        throw 'Subclasses must define a template path';
    }

    partials() {
        return {};
    }
}

class BaseView extends View {
    path() {
        return 'base.mustache';
    }
}

class IndexView extends View {
    path() {
        return 'index.mustache';
    }

    partials() {
        return {
            log: views.log.template
        }
    }
}

class LogView extends View {
    path() {
        return 'log.mustache';
    }
}

const views = {};

module.exports = function(tpl_dir) {
    template_dir = tpl_dir;

    views.base = new BaseView();
    views.index = new IndexView();
    views.log = new LogView();

    return views;
}
