const basename = require('path').basename;

class Tag {
  constructor(tag) {
    this.tag = tag;
  }

  toString() {
    return this.tag;
  }
}

class StopCommand extends Tag {
  toString() {
    return "Command: STOP";
  }
}

class PauseCommand extends Tag {
  toString() {
    return "Command: PAUSE";
  }
}

class NotFoundTag extends Tag {
  toString() {
    return `Unknown tag: ${this.tag}`
  }
}

class FileTag extends Tag {
  constructor(tag, path) {
    super(tag);
    this.path = path;
  }

  toString() {
    return basename(this.path);
  }
}

module.exports = {
  StopCommand: StopCommand
  , PauseCommand: PauseCommand
  , NotFoundTag: NotFoundTag
  , FileTag: FileTag
};

// vim:ts=2 sw=2 et:
