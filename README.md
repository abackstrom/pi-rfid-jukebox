# Rasperry Pi RFID Jukebox

Disclaimer: this is a perpetual work in progress!

This is a personal project of [Annika Backstrom][1] for using a Raspberry Pi as
an RFID-controlled "jukebox". Given an RPi with attached MFRC522, UIDs from 1K
MIFARE tags can be associated with media (MP3 files) and actions (currently,
STOP and PAUSE).

## Components

* Jukebox app with web frontend (node.js)
* RFID tag reader (Python)
* MP3 player (mpg321, not included)

## Setup

This section is incomplete (of course) and parts are tuned to my setup and
for my own convenience:

1. Clone project
2. Copy config.json.sample to config.json
3. Edit config.json for your setup
4. [Install Node.js](https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions)
4. `npm install` (node.js dependencies)
5. `virtualenv lib` # make a python virtualenv
6. `lib/bin/pip install -r requirements.txt`

## Development

This project supports Vagrant, if you don't want to install the packages
locally:

    vagrant up
    vagrant ssh -c 'cd /vagrant && npm run watch'

You can also just run from the checkout if your environment supports it.

`npm run watch` will use nodemon to restart the server when any file changes.

Dummy versions of `mpg321` and the tag reader are available in the `bin`
directory. These are node scripts, so you would include them in your
`config.json` like so:

    {
        "tag_reader": ["node", "./bin/tag-reader-dummy"],
        "mpg321": ["node", "./bin/mpg321-dummy"]
    }

## config.json

The following keys are available:

* `port` -- web server port
* `tag_reader` -- path to tag reader script. jukebox expects the tag reader to
  spit out a unique string for each tag (e.g. 10 character hex UID)
* `mpg321` -- path to mpg321 executable. should be run in "remote control" mode
  (`-R`), reading commands from stdin.
* `stop_id` -- id of the card that stops playback
* `pause_id` -- id of card that pauses playback
* `debug` -- true to enable some extra logging

  [1]: https://sixohthree.com/
