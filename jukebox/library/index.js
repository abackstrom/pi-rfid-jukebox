"use strict";

const EventEmitter = require('events').EventEmitter;
const tags = require('./tags');

class Library extends EventEmitter {
  constructor(config, backend) {
    super();

    this.config = config;
    this.backend = backend;
  }

  find(tag) {
    if (tag === this.config.stop_id) {
      this.emit('action', new tags.StopCommand(tag));
      return;
    }

    if (tag === this.config.pause_id) {
      this.emit('action', new tags.PauseCommand(tag));
      return;
    }

    this.backend.find(tag, path => {
      if (!path) {
        this.emit('action', new tags.NotFoundTag(tag));
        return;
      }

      this.emit('action', new tags.FileTag(tag, path));
    });
  }
}

module.exports.Library = Library;

// vim:set ts=2 sw=2 et:
