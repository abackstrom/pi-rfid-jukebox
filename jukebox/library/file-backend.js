"use strict";

const glob = require('glob');

function pickRandom(arr) {
  var index = Math.floor(Math.random() * arr.length);
  return arr[index];
}

class FileBackend {
  constructor(config) {
    this.config = config;
  }

  find(tag, callback) {
    // add extra * glob to the end of the file: old tag-reader got 10
    // character uids, new one gets 8 character for the same cards
    glob(this.config.media_path + '/**/' + tag + '* - *.mp3', (err, files) => {
      if (files.length > 0) {
        return callback(pickRandom(files));
      }
      callback();
    });
  }
}

module.exports = function(config) {
  return new FileBackend(config);
};

// vim:ts=2 sw=2 et:
