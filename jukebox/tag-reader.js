"use strict";

const ChildProcessEmitter = require('./child-process').ChildProcessEmitter;

class TagReader extends ChildProcessEmitter {
  constructor(config, logger) {
    super(config.tag_reader, logger);

    this.transform_line = l => l.toLowerCase();
  }
}

module.exports = function(config, logger) {
    return new TagReader(config, logger);
};

// vim:set ts=2 sw=2 et:
