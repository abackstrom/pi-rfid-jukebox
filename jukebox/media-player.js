"use strict";

const tags = require('./library/tags');

const { throttle } = require('throttle-debounce');

const { ChildProcessEmitter } = require('./child-process');

const DEFAULT_STOP_THROTTLE = 2000;
const DEFAULT_PAUSE_THROTTLE = 2000;
const DEFAULT_PLAY_THROTTLE = 5000;
const DEFAULT_UNKNOWN_THROTTLE = 2000;

class MediaPlayer extends ChildProcessEmitter {
  constructor(config, logger) {
    super(config.mpg321, logger);

    this.config = config;

    this.stderrFilters.push(line => {
      return line.substr(0, 3) == '@F '
    });

    this.stopThrottled = throttle(config.stop_throttle || DEFAULT_STOP_THROTTLE, true, this._stop);
    this.pauseThrottled = throttle(config.pause_throttle || DEFAULT_PAUSE_THROTTLE, true, this._pause);
    this.playFileThrottled = throttle(config.play_throttle || DEFAULT_PLAY_THROTTLE, true, this._playFile);

    // always throttled
    this.unknown = throttle(config.unknown_throttle || DEFAULT_UNKNOWN_THROTTLE, this._unknown);

    // default to unthrottled
    this.pause = this._pause;
    this.playFile = this._playFile;
    this.stop = this._stop;
  }

  throttle() {
    this.stop = this.stopThrottled;
    this.pause = this.pauseThrottled;
    this.playFile = this.playFileThrottled;
  }

  _stop(tag) {
    this.emit('command', tag);
    this.send('STOP');
  }

  _pause(tag) {
    this.emit('command', tag);
    this.send('PAUSE');
  }

  _playFile(tag) {
    this.emit('command', tag);
    this.send("LOAD " + tag.path);
  }

  _unknown(tag) {
    this.emit('command', tag);
  }

  handleTag(tag) {
    if (tag instanceof tags.StopCommand) {
      this.stop(tag);
    } else if (tag instanceof tags.PauseCommand) {
      this.pause(tag);
    } else if (tag instanceof tags.FileTag) {
      this.playFile(tag);
    } else {
      this.unknown(tag);
    }
  }
}

module.exports = function(config, logger) {
  return new MediaPlayer(config, logger);
};

// vim:set ts=2 sw=2 et:
