"use strict";

const { execFile } = require('child_process');
const glob = require('glob');
const util = require('util');

const globPromise = util.promisify(glob);

class ScriptRunner {
  constructor(config) {
    this.config = config;
  }

  find(tag) {
    return globPromise(this.config.script_path + '/**/' + tag + '*.sh').then(files => {
      return new Promise((resolve, reject) => {
        if (files.length === 1) {
          execFile(files[0]);
          resolve(true);
        }
        resolve(false);
      });
    });
  }
}

module.exports = function(config) {
  return new ScriptRunner(config);
};

// vim:ts=2 sw=2 et:
